import { Component, OnInit } from '@angular/core';
import { MatDialog} from '@angular/material/dialog';
import {EventEmitter} from 'events';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  

  constructor(public dialog: MatDialog) {
  }
 
  ngOnInit(): void {
    this.dialog.open(AppComponentDialog)
  }

   title = 'popup-project';
}
@Component({
  selector: 'app-root-dialog',
  templateUrl: 'app.component.dialog.html',
})
export class AppComponentDialog {
  constructor() {}
}